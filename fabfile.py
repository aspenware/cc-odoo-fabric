#!/usr/bin/env python

import fabric.api as fab
import fabric.colors as colors

import aspenware.io.remote.filesystem as remote


def development():
    fab.env.hosts = ['ec2-54-69-202-127.us-west-2.compute.amazonaws.com']
    fab.env.destination = '/home/ubuntu/'
    fab.env.symlink_target = "current"
    fab.env.service = 'odoo'
    fab.env.user = 'ubuntu'

def test_linux():
    fab.env.hosts = ['ec2-54-187-30-46.us-west-2.compute.amazonaws.com']
    fab.env.destination = '/home/ubuntu/'
    fab.env.symlink_target = "current"
    fab.env.service = 'odoo'
    fab.env.user = 'ubuntu'

def test_windows():
    fab.env.hosts = ['ec2-54-69-107-137.us-west-2.compute.amazonaws.com']
    fab.env.destination = '/cygdrive/c/Users/deploy/aspenware-odoo-runtime/'
    fab.env.symlink_target = 'source'
    fab.env.service = 'Odoo'
    fab.env.user = 'deploy'

def qa():
    fab.env.hosts = ['54.193.105.18', '54.183.225.70']
    fab.env.destination = '/cygdrive/c/Users/deploy/aspenware-odoo-runtime/'
    fab.env.symlink_target = 'source'
    fab.env.service = 'Odoo'
    fab.env.user = 'deploy'


def deploy(package):
    remote_path = fab.env.get('destination')
    with fab.cd(remote_path):
        fab.puts(colors.cyan("Operating within {0}".format(remote_path)))
        file_name = package.split("/")[-1]
        remote.upload_file(package, file_name)
        expanded_dir = remote.unpackage(file_name)
        remote.symlink(expanded_dir, fab.env.get('symlink_target'))
        remote.restart_service(fab.env.get('service'))


def undeploy(package):
    fab.puts(colors.cyan("Un-deploying {0}".format(package)))
    with fab.cd(fab.env.get('destination')):
        file_components = package.split('.')
        remote.remove_directory(file_components[0])
        remote.cleanup_artifacts(file_components[-1])
